#if UNITY_IOS && UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace Supercompany.GameCenterEntitlement
{
    public static class GameCenterEntitlementBuildPostProcessor
    {
        [PostProcessBuild(999)]
        private static void SetupXcode(BuildTarget target, string path)
        {
            if (target != BuildTarget.iOS)
            {
                return;
            }

            var pbxProjectPath = PBXProject.GetPBXProjectPath(path);
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);

            string entitlementsRelativePath = pbxProject.GetEntitlementFilePathForTarget(pbxProject.GetUnityMainTargetGuid()) ?? GetDefaultEntitlementsFileName();
            string entitlementsPath = $"{path}/{entitlementsRelativePath}";
            var entitlements = new PlistDocument();
            if (File.Exists(entitlementsPath))
            {
                entitlements.ReadFromFile(entitlementsPath);
            }

            entitlements.root.SetBoolean("com.apple.developer.game-center", true);
            entitlements.WriteToFile(entitlementsPath);

            if (!pbxProject.ContainsFileByProjectPath(entitlementsRelativePath))
            {
                pbxProject.AddFile(entitlementsPath, entitlementsRelativePath, PBXSourceTree.Source);
                pbxProject.AddCapability(pbxProject.GetUnityMainTargetGuid(), PBXCapabilityType.GameCenter, entitlementsRelativePath);
                pbxProject.WriteToFile(pbxProjectPath);
            }
        }

        // Taken from iOSNotificationPostProcessor implementation
        // Reference: https://github.com/Unity-Technologies/com.unity.mobile.notifications/blob/2.2.0/com.unity.mobile.notifications/Editor/iOSNotificationPostProcessor.cs#L99-L100
        public static string GetDefaultEntitlementsFileName()
        {
            var bundleIdentifier = PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS);
            return bundleIdentifier.Substring(bundleIdentifier.LastIndexOf(".") + 1) + ".entitlements";
        }
    }
}
#endif