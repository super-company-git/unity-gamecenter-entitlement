# GameCenter Entitlement
Editor script that automatically adds the GameCenter capability + entitlement to XCode projects in iOS builds.


## How to install
Either:
- Install this package via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html) using the following URL:
  ```
  https://gitlab.com/super-company-git/unity-gamecenter-entitlement.git#1.0.1
  ```
- Clone this repository inside your project's `Packages` folder
- Copy the [GameCenterEntitlementBuildPostProcessor](Editor/GameCenterEntitlementBuildPostProcessor.cs) script directly into you project's `Assets` folder